<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project ae.ae V0.3.90 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.12 -->
# kivy_app 0.3.108 - OUTDATED - merged into [new ae.kivy package](https://gitlab.com/ae-group/ae_kivy)

[![GitLab develop](https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_app/develop?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_app)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/ae-group/ae_kivy_app/release0.3.107?logo=python)](
    https://gitlab.com/ae-group/ae_kivy_app/-/tree/release0.3.107)
[![PyPIVersions](https://img.shields.io/pypi/v/ae_kivy_app)](
    https://pypi.org/project/ae-kivy-app/#history)

>ae namespace package portion kivy_app: main application classes and widgets for GUIApp-conform Kivy apps.

[![Coverage](https://ae-group.gitlab.io/ae_kivy_app/coverage.svg)](
    https://ae-group.gitlab.io/ae_kivy_app/coverage/index.html)
[![MyPyPrecision](https://ae-group.gitlab.io/ae_kivy_app/mypy.svg)](
    https://ae-group.gitlab.io/ae_kivy_app/lineprecision.txt)
[![PyLintScore](https://ae-group.gitlab.io/ae_kivy_app/pylint.svg)](
    https://ae-group.gitlab.io/ae_kivy_app/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/ae_kivy_app)](
    https://gitlab.com/ae-group/ae_kivy_app/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/ae_kivy_app)](
    https://gitlab.com/ae-group/ae_kivy_app/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/ae_kivy_app)](
    https://gitlab.com/ae-group/ae_kivy_app/)
[![PyPIFormat](https://img.shields.io/pypi/format/ae_kivy_app)](
    https://pypi.org/project/ae-kivy-app/)
[![PyPILicense](https://img.shields.io/pypi/l/ae_kivy_app)](
    https://gitlab.com/ae-group/ae_kivy_app/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/ae_kivy_app)](
    https://libraries.io/pypi/ae-kivy-app)
[![PyPIDownloads](https://img.shields.io/pypi/dm/ae_kivy_app)](
    https://pypi.org/project/ae-kivy-app/#files)


## installation


execute the following command to install the
ae.kivy_app package
in the currently active virtual environment:
 
```shell script
pip install ae-kivy-app
```

if you want to contribute to this portion then first fork
[the ae_kivy_app repository at GitLab](
https://gitlab.com/ae-group/ae_kivy_app "ae.kivy_app code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(ae_kivy_app):

```shell script
pip install -e .[dev]
```

the last command will install this package portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/ae-group/ae_kivy_app/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://ae.readthedocs.io/en/latest/_autosummary/ae.kivy_app.html
"ae_kivy_app documentation").
